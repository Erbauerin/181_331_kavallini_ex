#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "scl_controller.h"
#include "encriptioncontroller.h"
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);




    QQmlApplicationEngine engine;
//    Model model_qml;
      scl_controller controller;

      QQmlContext * context = engine.rootContext();
      // context->setContextProperty("model_qml", &model_qml);
      context->setContextProperty("controller", &controller);
      context->setContextProperty("model_qml", &controller.fileModel);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
