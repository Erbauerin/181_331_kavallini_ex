import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
//import QtMultimedia 5.12
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1
import QtQml.Models 2.12
import QtQuick 2.0


ApplicationWindow {
    id: general_Window // идентификатор окна(?) должен начинаться с МАЛЕНЬКОЙ буквы!!!
    // если к объекту не планируется обращаться, то id можно не задавать
    signal signalMakeRequestHTTP();
    visible: true
    width: 320 // ширина окна
    height: 480 // высота окна
    title: qsTr("Заметочки") // имя окошка, только в кавычках можно писать по-русски
    //qsTr - функция для переключения языка строки (если два языка, то нужно предоставить две строки)

    // Material.background: "#000000" // чёрный
    //Material.accent: "#16bf62" //насыщенно-зелёный
    property bool itisnew;
    property int ind: 0;
    property int old_ind;
    Connections{
        target: {model_qml; controller}

    }

    SwipeView {
        id: swipeView

        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page{ // страничка заметок
            id: view_note

            header: ToolBar{

                id: header_1
                Material.background: "#000000"

                Rectangle {
                    id: screen7
                    height: 30
                    width: parent.width
                    property int pixelSize: screen7.height
                    property color textColor: "#d9d9d9" //светло-серый

                    color: "#000000"
                    border.color: "#282828"
                    Row {
                        width: 300
                        height: 10

                        RoundButton {
                            //Material.background: "#000000"
                            Image{
                                source: "foto/menu.png"
                                height: 28
                                width: 28

                            }
                            flat: true
                            onClicked: {
                                drawer.open();
                            }
                        }
                        Text {
                            width: screen7.width
                            horizontalAlignment: Text.AlignHCenter

                            font.pixelSize: 17;
                            font.family: "lucida sans unicode";

                            color: screen7.textColor;
                            text: "Заметки"
                            anchors.top: parent.top
                        }
                    }
                }
            }



            //            ListModel{
            //                id: note_model

            ////                ListElement{
            ////                    index: 0
            ////                    name: "Заметка"
            ////                    note: "Содержание адлподвоаплдв ывдлполдыв ывлдлдывпдлв ывдлыалдв выадлавffffffffffffffff ddddddddddddddd fffffffffffffffff dddddddылд ывалва"
            ////                    tar: "💼" // 💼 - работа, 🔔 - напоминание, 🖊 - диплом
            ////                    colour: "желтый" // 9ee827 и bbf263 - для желтого, 3fc4a5 и b3f2e4 - для зеленого, 12cccc и 8aebeb - для голубого
            ////                }

            //            }





            GridView {
                anchors.fill: parent
                cellWidth: view_note.width/2; cellHeight: view_note.width/2
                Layout.leftMargin: 10
                model: model_qml //note_model
                delegate: Rectangle{
                    //height: view_note.width/2
                    width: view_note.width/2
                    Layout.margins: 5
                    Layout.leftMargin: 5
                    color: "#ccc"
                    ColumnLayout {
                        anchors.fill: parent
                        Label {
                            id: lab_t
                            text: {
                                if (tar == "Работа 💼")
                                    return "💼 " + name
                                else if (tar == "Диплом 🖊")
                                    return "🖊 " + name
                                else if (tar == "Напоминание 🔔")
                                    return "🔔 " + name
                            }
                            Layout.margins: 5
                            Layout.fillWidth: true
                            background: Rectangle {
                                anchors.fill: parent
                                color: {
                                    if (colour == "жёлтый")
                                        return "#9ee827"
                                    else if (colour == "зелёный")
                                        return "#3fc4a5"
                                    else if (colour == "голубой")
                                        return "#12cccc"
                                }
                            }

                            wrapMode: Label.WrapAtWordBoundaryOrAnywhere
                            maximumLineCount: 1
                            //Material.background: "#3fc4a5"
                            color: "#000000"

                            Button{
                                text: "+"
                                height: 15
                                width: 15
                                anchors.left: lab_t.right
                                onClicked: {
                                    swipeView.currentIndex = 1
                                    title_note_new.text = name
                                    desc_note.text = note
                                    old_ind = indexm-1
                                    itisnew = false
                                }
                            }
                        }

                        Rectangle {

                            Layout.fillWidth: true
                            Layout.margins: 5
                            height: 70
                            color: {
                                if (colour == "жёлтый")
                                    return "#bbf263"
                                else if (colour == "зелёный")
                                    return "#b3f2e4"
                                else if (colour == "голубой")
                                    return "#8aebeb"
                            }

                            Label{
                                text: note

                                width: parent.width
                                //Layout.fillHeight: true
                                color: "#000000"
                                wrapMode: Label.WrapAtWordBoundaryOrAnywhere
                                elide: Label.ElideRight
                                maximumLineCount: 4
                            }
                        }
                    }
                }
            }

            RoundButton {
                id: butt_new
                Image{
                    source: "foto/add.png"
                    height: 30
                    width: 30

                }
                flat: true
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                onClicked: {

                    swipeView.currentIndex = 1
                    title_note_new.clear()
                    desc_note.clear()
                    itisnew = true
                }
            }


        }


        //##################  РЕДАКТИРОВАНИЕ #################
        Page{
            id: redact

            header: ToolBar{

                id: header_2
                Material.background: "#000000"

                Rectangle {
                    id: screen
                    height: 30
                    width: parent.width
                    property int pixelSize: screen.height
                    property color textColor: "#d9d9d9" //светло-серый

                    color: "#000000"
                    border.color: "#282828"
                    Row {
                        width: 300
                        height: 10

                        RoundButton {
                            //Material.background: "#000000"
                            Image{
                                source: "foto/back.png"
                                height: 28
                                width: 28

                            }
                            flat: true
                            onClicked: {
                                ind--;
                                swipeView.currentIndex = 0;

                            }
                        }


                        Text {
                            width: screen.width
                            horizontalAlignment: Text.AlignHCenter

                            font.pixelSize: 17;
                            font.family: "lucida sans unicode";

                            color: screen.textColor;
                            text: "Редактирование"
                            anchors.top: parent.top
                        }
                    }
                }
            }


            Button { // кнопка сохранения изменений
                id: but_save
                Layout.alignment: Qt.AlignHCenter
                text: "Сохранить"
                font.pixelSize: 12
                Material.background: "#17bf98"
                Material.foreground: "#000"
                anchors.top: header_2.bottom
                anchors.right: parent.right


                onClicked: {
                    if(itisnew === true){ // если заметка новая
                        ind++;
                        controller.database_write(ind, title_note_new.text, desc_note.text, box.currentText, box2.currentText)
                        //controller.database_read()
                        //                        note_model.append({"indexm": ind, "name" : title_note_new.text,
                        //                                              "note" : desc_note.text,
                        //                                              "colour" : box.currentText,
                        //                                              "tar" : box2.currentText})
                        swipeView.currentIndex = 0


                    }
                    else{ // если редактируется уже существующая заметка
                        //                        note_model.set(old_ind, {"name" : title_note_new.text,
                        //                                           "note" : desc_note.text,
                        //                                           "colour" : box.currentText,
                        //                                           "tar" : box2.currentText})
                        swipeView.currentIndex = 0
                    }
                }
            }

            ComboBox { // выбор цвета заметок
                id: box
                editable: false
                anchors.left: parent.left
                anchors.top: but_save.bottom

                model: ListModel {
                    id: model
                    ListElement { text: "жёлтый" }
                    ListElement { text: "зелёный" }
                    ListElement { text: "голубой" }
                }
                onCurrentTextChanged: {
                    // 9ee827 и bbf263 - для желтого, 3fc4a5 и b3f2e4 - для зеленого, 12cccc и 8aebeb - для голубого
                    if (box.currentText === "жёлтый"){
                        field_t.color = "#9ee827"
                        area_t.color = "#bbf263"}
                    else if (box.currentText === "зелёный"){
                        field_t.color = "#3fc4a5"
                        area_t.color = "#b3f2e4"
                    }
                    else if (box.currentText === "голубой"){
                        field_t.color = "#12cccc"
                        area_t.color = "#8aebeb"
                    }
                }
            }


            ComboBox { // выбор тега
                id: box2
                editable: false
                anchors.left: box.right
                anchors.top: but_save.bottom
                model: ListModel {
                    id: model2
                    ListElement { text: "Работа 💼" }
                    ListElement { text: "Диплом 🖊" }
                    ListElement { text: "Напоминание 🔔" }
                }

            }

            Text{
                id: text_tit
                text: "Заголовок:"
                anchors.top: box.bottom
            }

            TextField{
                id: title_note_new
                font.pixelSize: 14
                font.bold: true
                maximumLength: 255
                background: Rectangle{
                    id: field_t
                    anchors.fill: parent
                }
                placeholderText: "Введите заголовок заметки..."
                anchors.top: text_tit.bottom
                wrapMode: Label.WrapAtWordBoundaryOrAnywhere
                width: redact.width
            }

            Text{
                id: text_desc
                text: "Текст заметки:"
                anchors.top: title_note_new.bottom
            }


            TextArea{
                id: desc_note
                width: redact.width
                font.pixelSize: 12
                background: Rectangle{
                    id: area_t
                    anchors.fill: parent
                }
                ScrollBar.vertical: ScrollBar { }
                placeholderText: "Введите текст заметки..."
                anchors.top: text_desc.bottom
                wrapMode: Label.WrapAtWordBoundaryOrAnywhere
            }




        }
    }



    Drawer{
        id: drawer
        width: 0.8 * parent.width
        height: parent.height

        ColumnLayout{
            width: drawer.width
            height: drawer.height
            Text{
                id: title_name
                text: "Заметки"
                font.pixelSize: 25;
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter

                // horizontalAlignment: Text.AlignHCenter
                font.family: "lucida sans unicode";
                color: "black";
                //anchors.top: parent.top
            }

            TextArea{
                id: title_description
                text: "Экзаменационное задание по дисциплине «Разработка безопасных мобильных приложенией»,\nМосковский Политех,\n30 июня 2020 г"
                font.pixelSize: 15;
                horizontalAlignment: Text.AlignHCenter
                font.family: "lucida sans unicode";
                color: "#616060";
                anchors.top: title_name.bottom
                wrapMode: Text.WordWrap  //TextArea.WrapAtWordBoundaryOrAnywhere
                width: drawer.width

            }
            Image {
                id: icon_mos
                source: "foto/mosp.jpg"
                Layout.fillHeight: true
                Layout.fillWidth: true
                width: drawer.width
                horizontalAlignment: Image.AlignHCenter
            }
            Text{
                id: email
                text: "Автор: bloody_jess@list.ru"
                font.pixelSize: 15;
                width: drawer.width
                anchors.leftMargin: 10
                anchors.bottom: top.repo
                horizontalAlignment: Text.AlignHCenter
                font.family: "lucida sans unicode";
                color: "black";
                anchors.top: icon_mos.bottom
            }
            Text{
                id: repo
                textFormat: Text.RichText
                wrapMode: TextArea.Wrap
                font.pixelSize: 17
                anchors.leftMargin: 10
                width: drawer.width
                horizontalAlignment: Text.AlignHCenter
                text: '<html><style type="text/css"></style><a href="https://gitlab.com/Erbauerin/181_331_kavallini_ex">репозиторий</a></html>'
                onLinkActivated: Qt.openUrlExternally(link)
                anchors.bottom: parent.bottom
            }
        }
    }

}
