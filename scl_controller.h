#ifndef SCL_CONTROLLER_H
#define SCL_CONTROLLER_H

#include <QObject>
#include <QAbstractListModel>

class FileObject
{
public:
    FileObject();
    FileObject(int indexm_1, QString name_1, QString note_1, QString colour_1, QString tar_1);

    int get_indexm() const;
    QString get_name() const;
    QString get_note() const;
    QString get_colour() const;
    QString get_tar() const;

private:
    int indexm;
    QString name;
    QString note;
    QString colour;
    QString tar;
};

class Model : public QAbstractListModel
{
    // Класс QAbstractItemModel обеспечивает:
            // 1. Хранение элементов модели в QList
            // 2. Трансляцию строковых запросов QML в адреса (свойств и методов) С++
            // 3. Интерактивность и обновление QML-связанных ...View посредством специальной сигнальной системы
   // Q_OBJECT
public:
    enum enumRoles {  // должно быть столько идентификаторов, сколько свойств в модели объекта
        indexm = Qt::UserRole + 1, name, note, colour, tar  // 256 зарезервировано для UserRole, начинаем с 257
    };

    explicit Model(QObject *parent = nullptr);

    void addItem(const FileObject & newItem);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;  // необходим, чтобы ...View заранее подготавливались к отрисовке (чтобы получать количество элементов списка)
    QVariant data(const QModelIndex & inde, int role = Qt::DisplayRole) const ;  // необходим для получения данных элемента index, соответствующих заданному с помощью role свойству элементу
    QVariantMap get(int idx) const;  // необходим для выгрузки i-го элемента целиком вместе со всеми свойствами
    void clear();  // очистка

private:
    QList<FileObject> objectlist;

protected:
    QHash<int, QByteArray> roleNames() const;

};


class scl_controller : public QObject
{
    //Q_OBJECT
public:
    explicit scl_controller(QObject *parent = nullptr);
   Model fileModel;


signals:
    void signalSendToQML(int indexm, QString name, QString note, QString colour, QString tar);
public slots:
    void database_read();
    void database_write(int indexm, QString name, QString note, QString colour, QString tar);
};

#endif // SCL_CONTROLLER_H
