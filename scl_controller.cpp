#include "scl_controller.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QAbstractListModel>
#include <iostream>
#include "encriptioncontroller.h"

scl_controller::scl_controller(QObject *parent) : QObject(parent)
{
    database_read();
}

void scl_controller::database_read()
{

    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE"); //добавляет базу данных в список соединений с базой данных
    database.setHostName("files");
   // Encriptioncontroller novoe;
//    novoe.decriptFile("09876543210987654321098765432345");

    database.setDatabaseName("C:/Kavallini_181_331/181_331_Kavallini/files.db");
    database.open();


    int indexm;
    QString name;
    QString note;
    QString colour;
    QString tar;
    if(database.isOpen())
    {
    if (fileModel.rowCount() > 0)
    {
    fileModel.clear();
    }

    QSqlQuery query;
    if (query.exec("SELECT * FROM files"))
    {
    query.exec("SELECT * FROM files");

    while (query.next())
    {
    indexm = query.value("File_indexm").toInt();
    name = query.value("File_name").toString();
    note = query.value("File_note").toString();
    colour = query.value("File_colour").toString();
    tar = query.value("File_tar").toString();

    fileModel.addItem(FileObject(indexm, name, note, colour, tar));
    }


    QSqlDatabase::removeDatabase("QSQLITE");
    database.close();
    }

    }
   //  emit signalSendToQML(indexm.toInt(), name, note, colour, tar);
}




void scl_controller::database_write(int indexm, QString name, QString note, QString colour, QString tar)
{


    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setHostName("files");
    database.setDatabaseName("C:/Kavallini_181_331/181_331_Kavallini/files.db");
    database.open();
    QSqlQuery query;
    query.exec("DROP TABLE files");
    query.exec("CREATE TABLE files("
    "File_indexm int,"
    "File_name varchar(255),"
    "File_note varchar(255),"
    "File_colour varchar(255),"
    "File_tar varchar(255))");

    query.prepare("INSERT INTO files(File_indexm, File_name, File_note, File_colour, File_tar)"
    "VALUES (:File_indexm, :File_name, :File_note, :File_colour, :File_tar)");
    query.bindValue(":File_indexm", indexm);
    query.bindValue(":File_name", name);
    query.bindValue(":File_note", note);
    query.bindValue(":File_colour", colour);
    query.bindValue(":File_tar", tar);
    query.exec();



    QSqlDatabase::removeDatabase("QSQLITE"); //удаляет соединение с БД
    database.close();
   // Encriptioncontroller novoe;
   // novoe.encriptFile("09876543210987654321098765432345");
    database_read();

}


FileObject::FileObject(int indexm_1, QString name_1, QString note_1, QString colour_1, QString tar_1)
    :indexm(indexm_1), name(name_1), note(note_1), colour(colour_1), tar(tar_1) // копирующий конструктор - для хранения данных
{

}

FileObject::FileObject()
{

}

Model::Model(QObject *parent) : QAbstractListModel(parent)
{

}

void Model::addItem(const FileObject &newItem)
{
    //часть сигнальной системы уведомления QML об изменениях модели. Передает в QML на какую позицию и сколько элементов появляется
    beginInsertRows(QModelIndex(), rowCount(), rowCount());  //нальный номер строки вставки и конечный номер строки вставки
    objectlist << newItem;  //вставка нового элемента
    endInsertRows();  //сообщает ListView о том, что изменение модели закончено
}

int Model::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return objectlist.count();
}

QVariant Model::data(const QModelIndex &inde, int role) const
{
    // метод для извлечения объекта и его определнного поля, заданных индексом списком и спецификатором role

    if (inde.row() < 0 || (inde.row() >= objectlist.count()))  // проверка на то что элемент списка не ниже нуля или не выше максимального элемента
        return QVariant();

    // получение ссылки на объект с нужным индексом
    const FileObject & itemToReturn = objectlist[inde.row()];

    // получение значения нужного поля выбранного объекта
    if (role == indexm)
        return itemToReturn.get_indexm();  // при любом return этой функции значение преобразуется в QVariant
    else if (role == name)
        return itemToReturn.get_name();
    else if (role == note)
        return itemToReturn.get_note();
    else if (role == colour)
        return itemToReturn.get_colour();
    else if (role == tar)
        return itemToReturn.get_tar();

    return QVariant();
}

QVariantMap Model::get(int idx) const
{
    // метод формирует выгрузку всех полей элемента списка под номером idx

    QVariantMap map;
    if (idx >= objectlist.count()) // проверка на то что элемент списка не ниже нуля или не выше максимального элемента
        return map;
    foreach (int i, roleNames().keys())
        map[roleNames().value(i)] = data(index(idx, 0), i);

    return map;
}

void Model::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);  // аналогично addItem()
    objectlist.clear();  // удаление элемментов списка
    endRemoveRows();  // аналогично addItem()
}

QHash<int, QByteArray> Model::roleNames() const
{
    // отвечает за заполнение словаря "строковое_имя" : роль
    QHash <int, QByteArray> roles;

    roles[indexm] = "indexm";  // в таком виде будет вызываться строка в QML
    roles[name] = "name";
    roles[note] = "note";
    roles[colour] = "colour";
    roles[tar] = "tar";

    return roles;
}

int FileObject::get_indexm() const
{
    return indexm;
}

QString FileObject::get_name() const
{
    return name;
}

QString FileObject::get_note() const
{
    return note;
}

QString FileObject::get_colour() const
{
    return colour;
}
QString FileObject::get_tar() const
{
    return tar;
}
